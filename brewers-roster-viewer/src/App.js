/*
Brewers Business Solutions Internship Evaluation
Created by: Ian Gresser
Date: 2-22-23

*/
import './App.css';
import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import {Accordion} from 'react-bootstrap';
const playerRoster = require('./data.json');//since the data is static, the data.json was imported directly into the script




/**
 * Author: Ian Gresser
 * Description: This is a standard React class that renders a component. If anyone that reads this is unfamiliar with React, it's a javascript 
 * library that allows the building, displaying, and manipulation of UI elements called components. It also uses React Bootstrap, an impolementation of the 
 * Bootstrap CSS framework that is usable in React
 */
class App extends React.Component{
  
  /**
   * Description: Standard React constructor
   * @param {no props sent in currently} props 
   */
  constructor(props){
    super(props);
    this.state = {
    }
    
  }

  /**
   * Description: The meat of the page. Each player's data is displayed in a react component. The component in question uses the React Bootstrap
   * element Accordion. An Accordion is a clickable element that will expand, allowing you to show more information. Clicking it again collapses it
   * @param {A single entry from the data.json} props 
   * @returns 
   */
  PlayerEntryBox = (props) =>{
    return(
      <div>
        <Accordion >
      <Accordion.Item  className='accrd' eventKey="0">
        <Accordion.Header>{/*This is the content viewable when Accordion is collapsed. It is still viewable when not collapsed */}
          <img alt={'Image of ' + props.playerData.firstName + " " + props.playerData.lastName} src={props.playerData.picture}></img>
          <p>{props.playerData.firstName}, {props.playerData.lastName}  Number: {props.playerData.number}</p>
          </Accordion.Header>
        <Accordion.Body>{/*This is the content viewable when Accordion is not collapsed */}
          <p>Player ID: {props.playerData.id}</p>
          <p>Birth Place: {props.playerData.birthCity}, {props.playerData.birthStateProvince}, {props.playerData.birthCountry}</p>
          <p>Primary Position:  {props.playerData.primaryPosition}</p>
          <p>Bat Side: {props.playerData.batSide}</p>
          <p>Throw Side: {props.playerData.throwSide}</p>
          
        </Accordion.Body>
      </Accordion.Item>
      
    </Accordion>
      </div>
    )
  }

  /**
   * This is a standard React class render function. In it, I use the map function to create a PlayerEntryBox created above per entry in the data.json.
   * All of them then get returned to index.js, which then sends the elements to the index.html in the public folder
   * @returns 
   */
  render(){
    return (
      <div className="App">
        {playerRoster.map((player, index) =>{
          return(
          <this.PlayerEntryBox className='plyrEntBx' key={index} playerData={player}/>
          )
      })}
      </div>
    );
  }

}

export default App;
